### README ###

# twine-reader-java #

This is a port of twinery core that let people access exported twinery html file and map it into list of object in Java.

The purpose of this project is to tweak twinery into story engine.

For more information about twinery, you can visit [twinery.org](http://twinery.org)

### How do I get set up? ###

Just import this project as general maven project.

### Testing ###

This uses JUnit as primary testing tool. 

You can make your own testing class(es) under test folder and run it using maven test or JUnit test.