package twine;

import java.util.ArrayList;

public class Passage {

	public class Link {

		public String lid = null;
		public String tag = null;

		public Link() {
			
		}

	}
	
	private ArrayList<Link> linkCollection = new ArrayList<Passage.Link>();
	private String pid = null;
	private String name = null;
	private String[] tags = null;
	private String text = null;
	private String left = null;
	private String top = null;

	public Passage() {

	}

	public void setId(String pid) {
		this.pid = pid;
	}

	public String getId() {
		return pid;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setTags(String[] tags) {
		this.tags = new String[tags.length];
		System.arraycopy(tags, 0, this.tags, 0, tags.length);
	}

	public String[] getTags() {
		return tags;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setLeft(String left) {
		this.left = left;
	}

	public String getLeft() {
		return left;
	}

	public void setTop(String top) {
		this.top = top;
	}

	public String getTop() {
		return top;
	}

	public void addLink(String lid, String tag) {
		Link link = new Link();
		link.lid = lid;
		link.tag = tag;
		linkCollection.add(link);
	}

	public Link getLink(int index) {
		return linkCollection.get(index);
	}

	public ArrayList<Link> getLinkCollection() {
		return linkCollection;
	}

	public int getLinkCollectionLength() {
		return linkCollection.size();
	}

}
