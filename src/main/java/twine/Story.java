package twine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.msgpack.MessagePack;
import org.msgpack.template.Templates;

public class Story {

	private String ifid = null;
	private String name = null;
	private String format = null;
	private String startNode = null;

	private HashMap<String, Passage> passageCollection = new HashMap<String, Passage>();

	public Story() {

	}

	public Story(String base64Binary) throws IOException {
		parseBinary(base64Binary);
	}

	public void setStartNode(String startNode) {
		this.startNode = startNode;
	}

	public String getStartNode() {
		return this.startNode;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getFormat() {
		return format;
	}

	public void setId(String ifid) {
		this.ifid = ifid;
	}

	public String getId() {
		return ifid;
	}

	public void addPassage(String pid, Passage passage) {
		passageCollection.put(pid, passage);
	}

	public Passage getPassage(String pid) {
		return passageCollection.get(pid);
	}

	@SuppressWarnings("null")
	public ArrayList<Passage> searchPassageByTags(String tag) {
		ArrayList<Passage> result = null;
		for (Passage passage : passageCollection.values())
			for (String e_tag : passage.getTags())
				if (e_tag.equals(tag))
					result.add(passage);
		return result;
	}

	@SuppressWarnings("null")
	public ArrayList<Passage> searchPassageByTags(String[] tags) {
		ArrayList<Passage> result = null;
		for (Passage passage : passageCollection.values())
			for (String e_tag : passage.getTags())
				for (String i : tags)
					if (e_tag.equals(i))
						result.add(passage);
		return result;
	}

	public HashMap<String, Passage> getPassageCollection() {
		return passageCollection;
	}

	public int getPassageCollectionLength() {
		return passageCollection.size();
	}

	public String getBinaryBase64() throws IOException {
		return Base64.encodeBase64String(getBinary());
	}

	public void parseBinary(String base64Binary) throws IOException {
		MessagePack msgpack = new MessagePack();
		List<String> storyData = msgpack.read(
				Base64.decodeBase64(base64Binary),
				Templates.tList(Templates.TString));

		// Parse story properties
		ifid = storyData.get(0);
		name = storyData.get(1);
		format = storyData.get(2);
		startNode = storyData.get(3);

		int passageCount = Integer.parseInt(storyData.get(4));
		int pointer = 5;

		// Iterate passage data
		for (int i = 0; i < passageCount; i++) {
			Passage passage = new Passage();
			
			String pid = storyData.get(pointer);
			passage.setId(pid);
			pointer++;
			
			String pname = storyData.get(pointer);
			passage.setName(pname);
			pointer++;
			
			int tagCount = Integer.parseInt(storyData.get(pointer));
			pointer++;
			String[] tags = new String[tagCount];
			for (int j = 0; j < tagCount; j++) {
				tags[j] = storyData.get(pointer);
				pointer++;
			}
			passage.setTags(tags);
			
			String text = storyData.get(pointer);
			passage.setText(text);
			pointer++;
			
			String left = storyData.get(pointer);
			passage.setLeft(left);
			pointer++;
			
			String top = storyData.get(pointer);
			passage.setTop(top);
			pointer++;
			
			int linkCount = Integer.parseInt(storyData.get(pointer));
			pointer++;
			for (int k = 0; k < linkCount; k++) {
				String lid = storyData.get(pointer);
				pointer++;
				String tag = storyData.get(pointer);
				pointer++;
				passage.addLink(lid, tag);
			}
			
			addPassage(passage.getName(), passage);
		}
	}

	public byte[] getBinary() throws IOException {
		byte[] result = null;
		List<String> storyData = new ArrayList<String>();
		MessagePack msgpack = new MessagePack();

		// Insert story properties into list
		storyData.add(getId());
		storyData.add(getName());
		storyData.add(getFormat());
		storyData.add(getStartNode());
		storyData.add(String.valueOf(getPassageCollectionLength()));

		for (Passage passage : passageCollection.values()) {
			storyData.add(passage.getId());
			storyData.add(passage.getName());
			storyData.add(String.valueOf(passage.getTags().length));
			for (String tag : passage.getTags())
				storyData.add(tag);
			storyData.add(passage.getText());
			storyData.add(passage.getLeft());
			storyData.add(passage.getTop());
			storyData.add(String.valueOf(passage.getLinkCollectionLength()));
			for (Passage.Link link : passage.getLinkCollection()) {
				storyData.add(link.lid);
				storyData.add(link.tag);
			}
		}

		// Serialize story properties into binary
		result = msgpack.write(storyData);

		// Encode binary data into base64 string
		return result;
	}

}
