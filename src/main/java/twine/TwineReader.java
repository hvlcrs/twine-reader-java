package twine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Base64;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.msgpack.MessagePack;
import org.msgpack.template.Templates;

public class TwineReader {

	private Document doc = null;
	private ArrayList<Story> storyCollection = new ArrayList<Story>();

	public enum selector {
		passage("tw-passage"), story("tw-story"), script("[role=script]"), stylesheet(
				"[role=stylesheet]"), storyData("tw-storydata"), passageData(
				"tw-passagedata");

		private String attribute;

		selector(String param) {
			this.attribute = param;
		}

		String getValue() {
			return this.attribute;
		}
	}

	public TwineReader() {

	}

	@SuppressWarnings({ "resource", "unused" })
	public void parseHtml(String path) throws IOException {
		// Load data from file
		String buffer = null;
		String fileContent = "";
		BufferedReader reader = new BufferedReader(new FileReader(path));

		while ((buffer = reader.readLine()) != null) {
			fileContent += buffer;
		}

		// Parse data into DOM
		doc = Jsoup.parse(fileContent);
		storyCollection.clear();

		Elements stories = doc.getElementsByTag(selector.storyData.getValue());
		for (Element e_story : stories) {
			String startPassageId = e_story.attr("startnode");

			// Create story object
			Story story = new Story();
			story.setStartNode(e_story.attr("startnode"));
			story.setName(e_story.attr("name"));
			story.setFormat(e_story.attr("format"));
			story.setId(e_story.attr("ifid"));

			// Create child passages
			Elements passages = e_story.getElementsByTag(selector.passageData
					.getValue());
			for (Element e_passage : passages) {
				String[] posBits = e_passage.attr("position").split(",");
				String[] tags = e_passage.attr("tags").trim().split(" ");
				String text = e_passage.text();

				Passage passage = new Passage();
				passage.setId(e_passage.attr("pid"));
				passage.setName(e_passage.attr("name"));
				passage.setTags(tags);
				passage.setText(text);
				passage.setLeft(posBits[0]);
				passage.setTop(posBits[1]);

				Pattern pattern = Pattern.compile("\\[+(.*?)\\]+");
				Matcher matcher = pattern.matcher(text);
				while (matcher.find()) {
					String link = matcher.group(1);
					String[] linkProperties = null;
					String lid = null;
					String tag = null;

					// Parse link format
					if (link.contains("->")) {
						linkProperties = link.split("(->)");
						lid = linkProperties[1];
						tag = linkProperties[0];
					} else if (link.contains("<-")) {
						linkProperties = link.split("(<-)");
						lid = linkProperties[0];
						tag = linkProperties[1];
					} else if (link.contains("|")) {
						linkProperties = link.split("(\\|)");
						lid = linkProperties[1];
						tag = linkProperties[0];
					}
					
					passage.addLink(lid, tag);
				}
				
				// Add passage into collection with unique name as a key
				story.addPassage(e_passage.attr("name"), passage);
			}

			// Add story into collection
			storyCollection.add(story);
		}
	}

	@SuppressWarnings({ "resource", "unused" })
	public void readBinary(String path) throws IOException {
		// Load data from file
		String buffer = null;
		String fileContent = "";
		BufferedReader reader = new BufferedReader(new FileReader(path));

		while ((buffer = reader.readLine()) != null) {
			fileContent += buffer;
		}
		
		// Decode base64 string data into binary
		MessagePack msgpack = new MessagePack();
		byte[] binaryData = Base64.decodeBase64(fileContent);
		
		// Deserialize binary data
		List<String> stories = msgpack.read(binaryData, Templates.tList(Templates.TString));
		storyCollection.clear();
		
		// Add story into story collection
		for(String base64Binary : stories) storyCollection.add(new Story(base64Binary));
	}

	public String getBinaryBase64() throws IOException {
		return Base64.encodeBase64String(getBinary());
	}

	public byte[] getBinary() throws IOException {
		byte[] result = null;
		List<String> stories = new ArrayList<String>();
		MessagePack msgpack = new MessagePack();
		
		for(Story story : storyCollection) {
			stories.add(story.getBinaryBase64());
		}
		
		// Serialize story collection into binary
		result = msgpack.write(stories);
		
		return result;
	}

	public Story getStory(int index) {
		return this.storyCollection.get(index);
	}

	public Story getStory(String name) {
		Story result = null;
		for (Story story : storyCollection) {
			if (story.getName().equals(name))
				return story;
		}

		return result;
	}

	public ArrayList<Story> getStoryCollection() {
		return this.storyCollection;
	}

	public int getStoryCollectionLength() {
		return this.storyCollection.size();
	}
	
}
